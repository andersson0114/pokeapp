from django.contrib import admin
from app.models import Entrenador, Maquina, Pokemon, PokemonM, Batalla
# Register your models here.

admin.site.register(Entrenador)
admin.site.register(Maquina)
admin.site.register(Pokemon)
admin.site.register(PokemonM)
admin.site.register(Batalla)