from django.contrib.admin.widgets import AutocompleteSelect, AdminDateWidget
from django.contrib import admin
from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.forms import UserCreationForm
from django.core.exceptions import ValidationError
from .models import Entrenador, Maquina, Pokemon, Batalla  #agregar los modelos que requieran formularios

class EntrenadorForm(forms.ModelForm): 

    password = forms.CharField(label = 'Conraseña', widget = forms.PasswordInput(
        attrs = {'class': 'form.control', 'type':'Password' ,'placeholder': 'Ingrese la Contraseña' }
    )) 

    class Meta:
        model = Entrenador
                 #Datos con los que se llenara el formulario (atributos de la tabla sin el ID)
        fields = ['nombreCompleto', 'userName', 'img', 'email',  'genero', 'equipo', 'password']
         
        widgets = {
            'nombreCompleto': forms.TextInput(
                attrs = {'class': 'form.control', 'placeholder': 'Ingrese tu nombre completo' }),
            'userName': forms.TextInput(
                attrs = {  'class': 'form.control', 'placeholder': 'Ingrese tu nombre de usuario' }),
            'img': forms.TextInput(
                attrs = {  'class': 'form.control', 'placeholder': 'urlImg' }),
            'email': forms.EmailInput(
                attrs = { 'class': 'form.control', 'placeholder': 'Ingrese el Correo' }),
            'genero': forms.Select(
                attrs = { 'class': 'form.control',  'placeholder': 'Elija su genero' }),
            'equipo': forms.Select(
                attrs = {'class': 'form.control', 'placeholder': 'Elija su equipo' }),
            
        }

class MaquinaForm(forms.ModelForm): 

    password = forms.CharField(label = 'Conraseña', widget = forms.PasswordInput(
        attrs = {'class': 'form.control', 'type':'Password' ,'placeholder': 'Ingrese la Contraseña' }
    )) 

    class Meta:
        model = Maquina
                 #Datos con los que se llenara el formulario (atributos de la tabla sin el ID)
        fields = ['userName', 'img', 'genero', 'equipo', 'password', 'poke1', 'poke2', 'poke3', 'poke4', 'poke5', 'poke6', 'dificultad']
         
        widgets = {
            'nombreCompleto': forms.TextInput(
                attrs = {'class': 'form.control', 'placeholder': 'Ingrese tu nombre completo' }),
            'img': forms.TextInput(
                attrs = {  'class': 'form.control', 'placeholder': 'urlImg' }),
            'userName': forms.TextInput(
                attrs = {  'class': 'form.control', 'placeholder': 'Ingrese tu nombre de usuario' }),
            'genero': forms.Select(
                attrs = { 'class': 'form.control',  'placeholder': 'Elija su genero' }),
            'equipo': forms.Select(
                attrs = {'class': 'form.control', 'placeholder': 'Elija su equipo' }),
            'poke1': forms.TextInput(
                attrs = {  'class': 'form.control', 'placeholder': 'pokeID' }),
            'poke2': forms.TextInput(
                attrs = {  'class': 'form.control', 'placeholder': 'pokeID' }),
            'poke3': forms.TextInput(
                attrs = {  'class': 'form.control', 'placeholder': 'pokeID' }),
            'poke4': forms.TextInput(
                attrs = {  'class': 'form.control', 'placeholder': 'pokeID' }),
            'poke5': forms.TextInput(
                attrs = {  'class': 'form.control', 'placeholder': 'pokeID' }),
            'poke6': forms.TextInput(
                attrs = {  'class': 'form.control', 'placeholder': 'pokeID' }),
            'dificultad': forms.Select(
                attrs = { 'class': 'form.control',  'placeholder': 'agregue una dificultad' }),
            
        }

class BatallaForm(forms.ModelForm): 

    class Meta:
        model = Batalla
                 #Datos con los que se llenara el formulario (atributos de la tabla sin el ID)
        #fields = ['id_maquina', 'Name_maquina', 'img_maquina', 'img_poke1', 'img_poke2', 'img_poke3', 'img_poke4', 'img_poke5', 'img_poke6', 'Estado']
        fields = [ 'Estado','Dificultad', 'img_maquina', 'Name_maquina']
          
        widgets = {
            'Estado': forms.TextInput(
                attrs = { 'class': 'form.control',  'placeholder': 'Estado' }), 
            'Dificultad': forms.TextInput(
                attrs = { 'class': 'form.control',  'placeholder': 'Dificultad' }),
            'img_maquina': forms.TextInput(
                attrs = { 'class': 'form.control',  'placeholder': 'img_maquina' }),
            'Name_maquina': forms.TextInput(
                attrs = { 'class': 'form.control',  'placeholder': 'Name_maquina' })
        }
  

class loginForm(AuthenticationForm): 
    def __init__(self, *args, **kwargs):
        super(loginForm, self).__init__(*args, **kwargs)

    userName = forms.CharField(widget = forms.TextInput(attrs={
        'class': 'form.control',
        'type' : 'text',
        'placeholder': 'Ingrese su nombre de usuario',
        })) 
    
    Contraseña = forms.CharField(widget = forms.TextInput(attrs={
        'class': 'form.control',
        'type' : 'password',
        'placeholder': 'Ingrese la Contraseña',
        })) 