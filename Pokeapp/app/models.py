from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager

# Create your models here.
opcion_genero = [
    [1,"Hombre"],
    [2,"Mujer"]
]

opcion_equipo = [
    [1,"Instinto"],
    [2,"Mistico"],
    [3,"Valor"]
]

opcion_dificultad = [
    [1,"Lider de Gimnasio"],
    [2,"Alto Mando"],
    [3,"Campeón"]
]
class UsuarioManager(BaseUserManager):
    def create_user(self, userName,  email, nombreCompleto, password = None):
        if not email:
            raise ValueError('El usuario debe tener correo electronico')

        usuario = self.model(
            userName = userName,  
            email = self.normalize_email(email),
            nombreCompleto = nombreCompleto )
        
        usuario.set_password(password)
        usuario.save()
        return usuario
    
    def create_superuser(self, userName,  email, nombreCompleto,  password):
        usuario = self.create_user(
            userName = userName, 
            email = self.normalize_email(email),
            nombreCompleto = nombreCompleto,  
            password= password)
        
        usuario.usuario_administrador = True
        usuario.save()
        return usuario

class Usuario(AbstractBaseUser):
    id = models.AutoField(primary_key=True)
    userName = models.CharField('Nombre de Usuario',max_length=50, blank=False, null=False) 
    genero =  models.IntegerField('Genero', choices = opcion_genero)  
    equipo =  models.IntegerField('Equipo', choices = opcion_equipo)  
    img = models.CharField('img',max_length=100, blank=False, null=False) 
    USERNAME_FIELD = 'userName'
    REQUIRED_FIELDS = [ 'userName', 'email','genero']


    class Meta: 
        abstract = True
        verbose_name = 'Usuario'
        verbose_name_plural= 'Usuarios'
        ordering = ['userName'] #se ordena según el parametro indicado(ej: nombre, apellido o cedula)

    
    def __str__(self):
        return f'{self.userName},{self.equipo}'

    def has_perm(self, perm, obj = None):
        return True
    
    def has_module_perms(self, app_label):
        return True
    
    @property
    def is_staff(self):
        return self.usuario_administrador

class Entrenador(Usuario):
    nombreCompleto = models.CharField('Nombres Completo',max_length=100, blank=False, null=False)
    usuario_administrador = models.BooleanField(default=False)  
    email = models.EmailField('Correo',max_length=50, blank=False, null=False)

    USERNAME_FIELD = 'userName'
    REQUIRED_FIELDS = ['nombreCompleto', 'email']

    class Meta:
        verbose_name = 'Entrenador'
        verbose_name_plural= 'Entrenadores'
        ordering = ['userName'] #se ordena según el parametro indicado(ej: nombre, apellido o cedula)

    
    def __str__(self):
        return f'{self.userName}'


class Maquina(Usuario):
    usuario_Maquina = models.BooleanField(default=True)  
    poke1 = models.CharField('poke1',max_length=100, blank=False, null=False)
    poke2 = models.CharField('poke2',max_length=100, blank=False, null=False)
    poke3 = models.CharField('poke3',max_length=100, blank=False, null=False)
    poke4 = models.CharField('poke4',max_length=100, blank=False, null=False)
    poke5 = models.CharField('poke5',max_length=100, blank=False, null=False)
    poke6 = models.CharField('poke6',max_length=100, blank=False, null=False)
    dificultad =  models.IntegerField('Dificultad', choices = opcion_dificultad)  
    USERNAME_FIELD = 'userName'
    REQUIRED_FIELDS = ['usuario_Maquina']

    class Meta:
        verbose_name = 'Maquina'
        verbose_name_plural= 'Maquinas' 
        ordering = ['userName'] 
    
   
class Batalla(models.Model):
    id_Batalla  = models.AutoField(primary_key=True)
    id_entrenador = models.ForeignKey(Entrenador, on_delete = models.CASCADE)
    id_maquina = models.CharField('id_maquina',max_length=100, blank=False, null=False) 
    Name_maquina = models.CharField('Name_maquina',max_length=100, blank=False, null=False) 
    img_maquina = models.CharField('img_maquina',max_length=100, blank=False, null=False) 
    #img_poke1 = models.CharField('img_poke1 ',max_length=100, blank=False, null=False) 
    #img_poke2 = models.CharField('img_poke2 ',max_length=100, blank=False, null=False) 
    #img_poke3 = models.CharField('img_poke3 ',max_length=100, blank=False, null=False) 
    #img_poke4 = models.CharField('img_poke4 ',max_length=100, blank=False, null=False) 
    #img_poke5 = models.CharField('img_poke5 ',max_length=100, blank=False, null=False) 
    #img_poke6 = models.CharField('img_poke6 ',max_length=100, blank=False, null=False) 
    Estado = models.CharField('Estado ',max_length=100, blank=False, null=False) 
    Dificultad = models.CharField('Dificultad ',max_length=100, blank=False, null=False)  
    class Batalla:
        verbose_name = 'Batalla'
        verbose_name_plural= 'Batallas'
        ordering = ['id_Batalla'] 

    def __str__(self):
        return f'{self.Name_maquina}'

class Pokemon(models.Model):
    id_entrenador  = models.ForeignKey(Entrenador, on_delete = models.CASCADE)
    pokenombre = models.CharField('pokenombre',max_length=100, blank=False, null=False)
    img = models.CharField('img',max_length=100, blank=False, null=False) 
    vida = models.CharField('vida',max_length=100, blank=False, null=False)
    ataque = models.CharField('ataque',max_length=100, blank=False, null=False)
    defensa = models.CharField('defensa',max_length=100, blank=False, null=False)
    mov1 = models.CharField('movimiento 1',max_length=100, blank=False, null=False)
    mov2 = models.CharField('movimiento 2',max_length=100, blank=False, null=False)
    mov3 = models.CharField('movimiento 3',max_length=100, blank=False, null=False)
    mov4 = models.CharField('movimiento 4',max_length=100, blank=False, null=False)

    class Pokemon:
        verbose_name = 'Pokemon'
        verbose_name_plural= 'Pokemons'
        ordering = ['pokenombre'] 

    def __str__(self):
        return f'{self.id_entrenador} - {self.pokenombre}'

class PokemonM(models.Model):
    id_entrenador  = models.ForeignKey(Maquina, on_delete = models.CASCADE)
    pokenombre = models.CharField('pokenombre',max_length=100, blank=False, null=False)
    img = models.CharField('img',max_length=100, blank=False, null=False) 
    vida = models.CharField('vida',max_length=100, blank=False, null=False)
    ataque = models.CharField('ataque',max_length=100, blank=False, null=False)
    defensa = models.CharField('defensa',max_length=100, blank=False, null=False)
    mov1 = models.CharField('movimiento 1',max_length=100, blank=False, null=False)
    mov2 = models.CharField('movimiento 2',max_length=100, blank=False, null=False)
    mov3 = models.CharField('movimiento 3',max_length=100, blank=False, null=False)
    mov4 = models.CharField('movimiento 4',max_length=100, blank=False, null=False)

    class Pokemon:
        verbose_name = 'PokemonM'
        verbose_name_plural= 'PokemonsM'
        ordering = ['pokenombre'] 

    def __str__(self):
        return f'{self.id_entrenador} - {self.pokenombre}'



