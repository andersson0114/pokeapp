const gengar = document.getElementById('gengar')  ;
const inputUsuario = document.getElementById('input-usuario');
const inputClave = document.getElementById('input-clave');
const body = document.querySelector('body');
const anchoMitad = window.innerWidth / 2;
const altoMitad = window.innerHeight / 2;
let seguirPunteroMouse = true;

body.addEventListener('mousemove', (m) => {
    if (seguirPunteroMouse) {
        if (m.clientX < anchoMitad && m.clientY < altoMitad) {
            gengar.setAttribute('src', '/static/app/img/idle/2.png');
        } else if (m.clientX < anchoMitad && m.clientY > altoMitad) {
            gengar.setAttribute('src', '/static/app/img/idle/3.png');
        } else if (m.clientX > anchoMitad && m.clientY < altoMitad) {
            gengar.setAttribute('src', '/static/app/img/idle/5.png');
        } else {
            gengar.setAttribute('src', '/static/app/img/idle/4.png');
        }
    }
})

id_username.addEventListener('focus',()=>{
    seguirPunteroMouse = false;
})

id_username.addEventListener('blur',()=>{
    seguirPunteroMouse = true;
})

id_username.addEventListener('keyup',()=>{
    let usuario = id_username["value"].length;
    if(usuario >= 0 && usuario<=5){
        gengar.setAttribute('src', '/static/app/img/read/1.png');
    }else if(usuario >= 6 && usuario<=14){
        gengar.setAttribute('src', '/static/app/img/read/2.png');
    }else if(usuario >= 15 && usuario<=20){
        gengar.setAttribute('src', '/static/app/img/read/3.png');
    }else{
        gengar.setAttribute('src', '/static/app/img/read/4.png');
    }
})

id_password.addEventListener('focus',()=>{
    seguirPunteroMouse = false;
    let cont = 1;
    const cubrirOjo = setInterval(() => {
        gengar.setAttribute('src','/static/app/img/cover/'+cont+'.png');
        if(cont < 5){
            cont++;
        }else{
            clearInterval(cubrirOjo);
        }
    }, 60);
})

id_password.addEventListener('blur',()=>{
    seguirPunteroMouse = true;
    let cont = 4;
    const descubrirOjo = setInterval(() => {
        gengar.setAttribute('src','/static/app/img/cover/'+cont+'.png');
        if(cont > 1){
            cont--;
        }else{
            clearInterval(descubrirOjo);
        }
    }, 60);
})
