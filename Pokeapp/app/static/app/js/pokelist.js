//const pokemonContainer = document.querySelector(".pokemon-container");
const cards = document.getElementById('cards') //cards
const templateCard= document.getElementById('template-card').content    
const fragment = document.createDocumentFragment() //fragment
const spinner = document.querySelector("#spinner")
const next =  document.getElementById("next")
const back =  document.getElementById("back")
const b1 =  document.getElementById("b1")
const b2 =  document.getElementById("b2")
const b3 =  document.getElementById("b3")
const b4 =  document.getElementById("b4")
const b5 =  document.getElementById("b5")
const num = document.getElementsByClassName("num")
let search =  document.getElementById("search")
let equipo = {}
let integrantes = 1
let offset = 1;
let limit = 6;
let n = 1; 
let url;


cards.addEventListener('click', e =>{
    addEquipo(e)
})

back.addEventListener('click', () =>{
    if(offset!=1){
        offset -= 7 ;
        boton = parseInt(document.getElementById("b1").textContent) ; 
        cambio(1);
         
    }
})

next.addEventListener('click', () =>{
    offset += 7;
    boton = parseInt(document.getElementById("b2").textContent) ; 
    cambio(5); 
    
})
 

function cambio(n){
    console.log(n);
    boton = parseInt(document.getElementById("b"+n).textContent) ; 
 
    if(boton>2){
        b1.textContent = boton-2;
        b2.textContent = boton-1;
        b3.textContent = boton;
        b4.textContent = boton+1;
        b5.textContent = boton+2;
    }else{
        b1.textContent = 1;
        b2.textContent = 2;
        b3.textContent = 3;
        b4.textContent = 4;
        b5.textContent = 5;
    }


    offset += (boton*7)
    removeChildNode(cards);
    fetchPokemons(offset, limit);
}
 
const fetchPokemons = async(offset, limit) =>{
    spinner.style.display = "block";
    try {
        const res= await fetch('https://pokeapi.co/api/v2/pokemon?limit='+limit+'&offset='+offset)
        const data = await res.json()  
        creaPokemon(data) 
         
             
    } catch (error) {
        console.log(error)
    }
}
const fetchPokemon =async(id)=>{
    try {
        const res= await fetch('https://pokeapi.co/api/v2/pokemon/'+id+'/')
        const data = await res.json() 
        pokedata(data)
             
    } catch (error) {
        console.log(error)
    }
    
} 

const creaPokemon = data =>{
    pokemons = data.results;
    pokemons.forEach(pokemon => { 
        poke = fetchPokemon(pokemon.name);
        
    } );
    
}

const pokedata = data =>{
    
    spinner.style.display = "none";
    templateCard.querySelector('h4').textContent = data.name
    templateCard.getElementById("Hp").textContent =  data.stats[0].base_stat; 
    templateCard.getElementById("Attack").textContent =  data.stats[1].base_stat;
    templateCard.getElementById("Defense").textContent = data.stats[2].base_stat;
    templateCard.querySelector('img').setAttribute("src", data.sprites.other.home.front_default)
    templateCard.querySelector('.btn-primary').dataset.id = data.name 
    const clone = templateCard.cloneNode(true)
    fragment.appendChild(clone)
    cards.appendChild(fragment)
}

function removeChildNode(parent){
    while(parent.firstChild){
        parent.removeChild(parent.firstChild);
    }
}

search.addEventListener('click', () =>{
    let id = document.getElementById("Buscar").value
    removeChildNode(cards);
    if(id == 0){
        fetchPokemons(offset, limit);
    }else{
        fetchPokemon(id);
    }
    
    document.getElementById("Buscar").value = ''
    
    
})

const addEquipo = e => {
    if(e.target.classList.contains('btn-primary')){
        
        setEquipo(e.target.parentElement);
        
    }
    
    e.stopPropagation()
}

const setEquipo = objeto =>{ 
    
    if(integrantes<7){
        let poid = document.querySelector("#id_poke"+integrantes) //input
        let dpoid = document.querySelector("#poke"+integrantes) //div - input
        const poke = objeto.querySelector('.btn-primary').dataset.id
        equipo[integrantes]={...poke}
        integrantes++;
        poid.value = poke
        dpoid.style.display = "block";
        poid.style.display = "block"; 
        //poid.focus(); 
        //poid.disabled = true;
    }
    
    console.log(equipo)
}   


 
function nextImg(){
    n++;
    if(n>20){
       n=1;
    } 
    setImge()
}

function previusImg(){
    n--;
    if(n<1){
       n=20;
    }
    setImge()  
}

const setImge = () =>{
    url = "/static/app/img/Avatares/"+n+".jpg"
    document.getElementById('img').setAttribute("src", url );
    document.getElementById('id_img').value = url;
} 

setImge()
fetchPokemons(offset, limit);