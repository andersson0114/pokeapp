from django.urls import path
from .views import Index, RegistroEntrenador,registroMaquina,Pokemones,Battle, Historial, pokeCambio,verMaquinas,Home

app_name = 'app'
urlpatterns = [
    path('', Index, name="Index"),
    path('RegistroEntrenador', RegistroEntrenador, name= "RegistroEntrenador"),
    path('registroMaquina', registroMaquina, name= "registroMaquina"),
    path('Pokemones-<id>', Pokemones, name= "Pokemones"),
    path('Battle-<id>', Battle, name= "Battle"),
    path('Historial-<id>', Historial, name= "Historial"),
    path('pokeCambio-<id>', pokeCambio, name= "pokeCambio"),
    path('pokeCambio', pokeCambio, name= "pokeCambio"),
    path('verMaquinas', verMaquinas, name= "verMaquinas"),
    path('Home-<id>', Home, name= "Home"),
    
    
]