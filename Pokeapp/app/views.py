from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy
from django.contrib import messages
from django.contrib.auth.forms import UserCreationForm 
from django.views.generic import CreateView, ListView, UpdateView, DeleteView
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User 
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from .models import Entrenador, Pokemon, Maquina, PokemonM, Batalla #Models
from .forms import EntrenadorForm, MaquinaForm , BatallaForm #Formularios
import  json, requests,random


##
@login_required
def Index(request):
    if  request.user.is_authenticated:
        usuario = request.user.username
    else:
        usuario = None

    if usuario:
        try:
            usuario_db = Entrenador.objects.get(userName=usuario)
            equipo = usuario_db.equipo
            genero = usuario_db.genero
            id_entrenador= usuario_db.id
            pokemons = Pokemon.objects.filter(id_entrenador = id_entrenador)
            admin = usuario_db.usuario_administrador 
        except :
            equipo = None
            genero = None 
    else:
        equipo = None
        genero = None 

        
    data = {
            'usuario':usuario_db,
            'equipo': equipo,
            'genero': genero ,
            'pokemons': pokemons,
            'admin':admin,
            'id':id_entrenador,
            }
    print(admin)
    if admin == True:
        print("administrador")
        return registroMaquina(request)
    else:
        return render(request, 'app/Index.html',data)


def Home (request, id):
    try:
        usuario_db = Entrenador.objects.get(id=id)
        equipo = usuario_db.equipo
        genero = usuario_db.genero
        id_entrenador= usuario_db.id
        pokemons = Pokemon.objects.filter(id_entrenador = id_entrenador) 
    except :
        equipo = None
        genero = None 
    
    data = {
            'usuario':usuario_db,
            'equipo': equipo,
            'genero': genero ,
            'pokemons': pokemons, 
            'id':id_entrenador,
            }
    return render(request, 'app/Home.html',data)

def query_pokeapi(resource_url, id):
    BASE_URL = 'http://pokeapi.co'
    url = '{0}{1}'.format(BASE_URL, resource_url+str(id)+'/')
    response = requests.get(url)

    if response.status_code == 200:
        return json.loads(response.text)
    return None


def RegistroEntrenador(request):
    gen= []
    gen.append(25)
    form_class = EntrenadorForm()
    data = {
            'form':form_class,
            }

    if request.method == 'GET':
        return render(request,'registration/RegistroEntrenador.html',data)
    else:
        form_class = EntrenadorForm(request.POST)
        if form_class.is_valid():
            userName = request.POST['userName']
            password = request.POST['password']
            email = request.POST['email']
            nombreCompleto = request.POST['nombreCompleto']
            genero = request.POST['genero']
            imag = request.POST['img']
            equipo = request.POST['equipo']
            user = User.objects.create_user(username=userName, email=email,  password=password, first_name=nombreCompleto)
            user.save()
            form_class.save()   

            ###
            while len(gen) < 6:
                repetido = True
                a = random.randint(1,898)
                for i in gen:
                    if i == a:
                        repetido = False

                if repetido:
                    gen.append(a)
            
            for i in gen:
                pokemon = query_pokeapi('/api/v1/pokemon/', i)
                nombre = pokemon['name']
                vida = pokemon['stats'][0]['base_stat']
                defensa = pokemon['stats'][1]['base_stat']
                ataque = pokemon['stats'][2]['base_stat']
                mov1 = pokemon['moves'][0]['move']['name']
                mov2 = pokemon['moves'][1]['move']['name']
                mov3 = pokemon['moves'][2]['move']['name']
                mov4 = pokemon['moves'][3]['move']['name']
                img = pokemon['sprites']['other']['home']['front_default']
                id_entrenador= Entrenador.objects.get(email = email)
 
                pokemon = Pokemon.objects.get_or_create(id_entrenador= id_entrenador, pokenombre=nombre, img=img, vida =vida, ataque=ataque, defensa =defensa,  mov1 =mov1, mov2 =mov2, mov3 =mov3, mov4 =mov4 )
            return redirect('accounts/login/')
        return render(request,'registration/RegistroEntrenador.html',data) 

@login_required
def registroMaquina(request):
    form_class = MaquinaForm()
    pokemones = []
    data = {
            'form':form_class,
            }

    if request.method == 'GET':
        return render(request,'app/registroMaquina.html',data)
    else:
        try:
            form_class = MaquinaForm(request.POST) 
            if form_class.is_valid():
                userName = request.POST['userName']
                genero = request.POST['genero']
                equipo = request.POST['equipo']
                dificultad = request.POST['dificultad']
                password = request.POST['password']
                poke1 = request.POST['poke1']
                poke2 = request.POST['poke2']
                poke3 = request.POST['poke3']
                poke4 = request.POST['poke4']
                poke5 = request.POST['poke5']
                poke6 = request.POST['poke6']
                maquina =  User.objects.create_user(username=userName,password=password) 
                maquina.save()
                messages.success(request, "registro exitoso")
                form_class.save()   
                id_entrenador= Maquina.objects.get(userName = userName) 
                pokemones = [poke1,poke2,poke3,poke4,poke5,poke6] 

                for i in pokemones: 
                    pokemon = query_pokeapi('/api/v1/pokemon/', i)
                    nombre = pokemon['name']
                    vida = pokemon['stats'][0]['base_stat']
                    defensa = pokemon['stats'][1]['base_stat']
                    ataque = pokemon['stats'][2]['base_stat']
                    mov1 = pokemon['moves'][0]['move']['name']
                    mov2 = pokemon['moves'][1]['move']['name']
                    mov3 = pokemon['moves'][2]['move']['name']
                    mov4 = pokemon['moves'][3]['move']['name']
                    img = pokemon['sprites']['other']['home']['front_default']

                    print (nombre, vida, defensa, ataque, mov1, mov2, mov3, mov4, img )
                    pokemon = PokemonM.objects.get_or_create(id_entrenador= id_entrenador, pokenombre=nombre, img=img, vida =vida, ataque=ataque, defensa =defensa,  mov1 =mov1, mov2 =mov2, mov3 =mov3, mov4 =mov4 ) 
                            
                return redirect('app:registroMaquina')
        except :
            messages.success(request, "Ha ocurrido un error. Intentelo de nuevo")
    return  render(request,'app/registroMaquina.html',data) 

@login_required
def verMaquinas(request):
    maquinas = Maquina.objects.all()

    data = {

        'maquinas': maquinas
    }
    
    return  render(request,'app/verMaquinas.html', data) 


@login_required
def Pokemones(request, id):
    usuario_db = Entrenador.objects.get(id=id)
    equipo = usuario_db.equipo
    pokemons = Pokemon.objects.filter(id_entrenador = id)
    data = { 
            'pokemons': pokemons, 
            'id':id,
            'equipo':equipo
            }
    return  render(request,'app/pokemones.html', data)

@login_required

def Historial(request,id):
    batallas = Batalla.objects.filter(id_entrenador = id)
    usuario_db = Entrenador.objects.get(id=id)
    equipo = usuario_db.equipo

    data = { 
            'batallas': batallas, 
            'id':id,
            'equipo':equipo
            }
    return  render(request,'app/pokeHistorial.html',data) 


@login_required
def Battle(request, id):
    busqueda= []
    pokesJ = []
    pokesM = []
    queryset = request.GET.get("Buscar")
    maquinas = Maquina.objects.all() 

    if queryset:
        maquinas = Maquina.objects.filter(
            Q(dificultad = queryset )
        ).distinct()

    for i in maquinas: 
        busqueda.append(i)


    a = len(busqueda)
    b = random.randint(1,a)
    print(queryset)
    maqui = busqueda[b]

    if request.method == 'POST': 
        queryset = request.GET.get("Buscar")
        formulario = BatallaForm(request.POST)
        usuario_db = Entrenador.objects.get(id=id)
        
        if formulario.is_valid():
            
            id_entrenador= usuario_db
            id_maquina = maqui.id
            Name_maquina = request.POST['Name_maquina']
            Dificultad = request.POST['Dificultad']
            Estado = request.POST['Estado']
            img_maquina = request.POST['img_maquina']
            print("identidicacion",id_entrenador) 

            batalla =  Batalla.objects.get_or_create(id_entrenador=id_entrenador, id_maquina=id_maquina, Name_maquina=Name_maquina, Estado=Estado, Dificultad=Dificultad, img_maquina=img_maquina)
             
            return redirect ('app:Historial', id)

    else:
        form = BatallaForm()
        usuario_db = Entrenador.objects.get(id=id)
        id_entrenador= usuario_db.id
        pokemonsJ = Pokemon.objects.filter(id_entrenador = id_entrenador)
        PokemonsM = PokemonM.objects.filter(id_entrenador = maqui ) 
        equipo = usuario_db.equipo

        for i in pokemonsJ:
            pokesJ.append(i)

        for i in PokemonsM:
            pokesM.append(i)

        data = {
            'form': form,
            'maquina': maqui,
            'usuario': usuario_db, 
            'Jp1': pokesJ[0],
            'Jp2': pokesJ[1],
            'Jp3': pokesJ[2],
            'Jp4': pokesJ[3],
            'Jp5': pokesJ[4],
            'Jp6': pokesJ[5], 
            'Mp1': pokesM[0],
            'Mp2': pokesM[1],
            'Mp3': pokesM[2],
            'Mp4': pokesM[3],
            'Mp5': pokesM[4],
            'Mp6': pokesM[5], 
            'equipo':equipo
        }

        return  render(request,'app/battle.html', data) 

@login_required
def pokeCambio(request, id):
    return  render(request,'app/pokeCambio.html')

def registro(pokemones, id_entrenador):
    for i in pokemones:
        pokemon = query_pokeapi('/api/v1/pokemon/', i)
        nombre = pokemon['name']
        vida = pokemon['stats'][0]['base_stat']
        defensa = pokemon['stats'][1]['base_stat']
        ataque = pokemon['stats'][2]['base_stat']
        mov1 = pokemon['moves'][0]['move']['name']
        mov2 = pokemon['moves'][1]['move']['name']
        mov3 = pokemon['moves'][2]['move']['name']
        mov4 = pokemon['moves'][3]['move']['name']
        img = pokemon['sprites']['other']['home']['front_default']
        id_entrenador= id_entrenador

        print (nombre, vida, defensa, ataque, mov1, mov2, mov3, mov4, img )
        pokemon = PokemonM.objects.get_or_create(id_entrenador= id_entrenador, pokenombre=nombre, img=img, vida =vida, ataque=ataque, defensa =defensa,  mov1 =mov1, mov2 =mov2, mov3 =mov3, mov4 =mov4 )