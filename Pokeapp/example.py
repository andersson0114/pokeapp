
from fastapi import FastAPI
import sqlite3 as sql
import pandas as pd
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder

app = FastAPI()
connection = sql.connect("biblio")


@app.get('/',
tags=["Este método permite buscar entrenadores por id"])
async def buscar_entrenador(id: int):
    query = pd.read_sql_query(f"SELECT * FROM app_entrenador WHERE id = {id}", connection)
    response = query.to_dict(orient='records') 
    # response = json.dumps(response[0])
    try:
        response = jsonable_encoder(response[0])
    except:
        response = {}
    return JSONResponse(content=response)