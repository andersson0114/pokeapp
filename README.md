<h1 align="center"> Poke App </h1> <br>

<p align="center"> 
    <img alt="EduText" title="EduText" src="https://user-images.githubusercontent.com/70853111/202832667-76a1da57-9768-4d7c-af81-3bd4fe051786.png" width="540">
</p> 

  


<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Table of Contents

- [Introduccion](#Introduccion)
- [Características](#Características)
- [Registro y Logueo](#Register)
- [Tipos de Usuario:](#Usuarios)
- [Home](#Home) 
- [Mi Equipo](#Equipo) 
- [Batallas](#Batallas)
- [Historial de Batallas](#Historial)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Introduccion

Este proyecto de practica fue creado consumiendo la pokeapi existente en internet, además de traer ciertas interacciones que serán explicadas posteriormente.

## Características

Algunas de sus caracteristicas son:

* Registro de y Logueo de Usuario.
	*  El registro de usuario le agregaba aleatoreamente 6 pokemones al nuevo usuario, y este podia elegir 3 tipos de usuarios:
    * 1- Instinto.
    * 2- Mistico.
    * 3- Valiente.
* Perfil.
	* El perfil nos mostrará un NavBar lateral que tendrá un color segun el tipo de usuario(Amarillo, Azul o Rojo), ademas de mostrarnos la información del usuario y la información basica de sus Pokemones
* Pokemon.
	* Esta sección nos mostrará las datos de cada Pokemón, como el nombre, la vida, defensa, ataque y ataques especiales.
* Batallas.
	* En esta sección se podrá elegir una dificultad de batalla y empezar un duelo sencillo que depende de dos numeros aleatorios, el jugador que saque el numero de mayor valor gana el duerlo, y el que gane la mayor cantidad de duelos gana la batalla.
* Historial de Batallas.
	* En esta sección se mostrará los datos de cada batalla del usuario, tales como la imagen y el nombre del contrincante, la dificultad y el resultado.



## Register
<p align="center"> 
    <img alt="EduText" title="EduText" src="https://user-images.githubusercontent.com/70853111/202832980-b6afce2a-d048-4cdf-a43f-5be512f4cbfa.png" width="540">
</p> 

<p align="center"> 
    <img alt="EduText" title="EduText" src="https://user-images.githubusercontent.com/70853111/202832999-88716714-acb5-454a-8cf7-f4c321d83c55.png" width="540">
</p> 

## Usuarios
  * Mistico 
<p align="center"> 
    <img alt="EduText" title="EduText" src="https://user-images.githubusercontent.com/70853111/202832876-842b659b-0609-4b9e-833a-5365bc1dddfb.png" width="540">
</p> 

  * Instinto 
<p align="center"> 
    <img alt="EduText" title="EduText" src="https://user-images.githubusercontent.com/70853111/202832929-57804563-d419-4630-8273-1d13c4f214f2.png" width="540">
</p> 

  * Valor 
<p align="center"> 
    <img alt="EduText" title="EduText" src="https://user-images.githubusercontent.com/70853111/202833011-5ed04aae-a75d-4aef-ac61-c81df7b1c5d3.png" width="540">
</p> 

 

## Home
<p align="center"> 
    <img alt="EduText" title="EduText" src="https://user-images.githubusercontent.com/70853111/202833070-5451d26c-693c-46cc-8883-83dce0d83bd4.png" width="540">
</p> 

<p align="center"> 
    <img alt="EduText" title="EduText" src="https://user-images.githubusercontent.com/70853111/202833407-db70ac6d-5190-4e77-a062-1d98d56e0965.png" width="540">
</p> 

## Equipo
<p align="center"> 
    <img alt="EduText" title="EduText" src="https://user-images.githubusercontent.com/70853111/202833031-f91f6a95-b779-41d7-bfff-e99e1ff05e02.png" width="540">
</p> 
 

## Batallas
<p align="center"> 
    <img alt="EduText" title="EduText" src="https://user-images.githubusercontent.com/70853111/202833085-411f2a62-0ed8-4b2a-bbd6-0f69d0d37523.png" width="540">
</p> 
<p align="center"> 
    <img alt="EduText" title="EduText" src="https://user-images.githubusercontent.com/70853111/202833107-f2d7b5a8-93ae-4288-b293-d80543b4d1cd.png" width="540">
</p> 
<p align="center"> 
    <img alt="EduText" title="EduText" src="https://user-images.githubusercontent.com/70853111/202833131-0af191e7-2924-4aec-8ff4-1c45ff412d9f.png" width="540">
</p> 
<p align="center"> 
    <img alt="EduText" title="EduText" src="https://user-images.githubusercontent.com/70853111/202833144-4c3082ce-efe0-4f41-8e56-09c8bd186112.png" width="540">
</p> 
<p align="center"> 
    <img alt="EduText" title="EduText" src="https://user-images.githubusercontent.com/70853111/202833162-3f724304-984e-46ec-9033-f660cf17181c.png" width="540">
</p> 
  
 
## Historial
<p align="center"> 
    <img alt="EduText" title="EduText" src="https://user-images.githubusercontent.com/70853111/202833176-8c3f5857-85a6-4fb4-9b51-2c590e1321cd.png" width="540">
</p> 
 

  
## Colaboradores 

 [<img src="https://avatars.githubusercontent.com/u/70853111?v=4" width="100px;"/><br /><sub><b>Andersson Cordoba</b></sub>](https://github.com/andersson980114)<br /> 
 [<img src="https://avatars.githubusercontent.com/u/33579122?v=4" width="100px;"/><br /><sub><b>Laura Jimenez</b></sub>](https://github.com/totoro1298)<br />
 [<img src="https://avatars.githubusercontent.com/u/37989077?v=4" width="100px;"/><br /><sub><b>Diego Toro</b></sub>](https://github.com/Dtorofl)<br /> 
